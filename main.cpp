#include <iostream>

class Animal {
private:
    std::string name;
public:

    void setName(std::string newName){
        name = newName;
    }

    void greet() {
        std::cout << "Hi from " << name << std::endl;
    }
};

int main() {

    Animal animal;
    animal.setName("Abstract Cat");
    animal.greet();

    Animal dog;
    dog.setName("Abstract Dog");
    dog.greet();

    return 0;
}
